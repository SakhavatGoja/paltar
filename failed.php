<?php
    include("includes/head.php");
?>
    <section class="failed">

        <?php
            include("includes/header.php");
        ?>
        
        <div class="failed-container w-100">
            <div class="container">
                <div class="row">
                    <div class="failed-box">
                        <div class="failed-img">
                            <img src="./img2/failed.png" alt="">
                            <p>
                                Səhifə tapılmadı
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
            include("includes/footer.php");
        ?>
    </section>
   
<?php
    include("includes/script.php");
?>