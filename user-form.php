<?php
    include("includes/head.php");
?>

    
    <section class="form-contents">
        <?php
            include("includes/header.php");
        ?>

        <div class="form-container w-100">
            <div class="container">
                <div class="row">
                    <div class="user-form-box">
                        <div class="user-form account">
                            <div class="user-form-header">
                                <a href="#"  class="login active-form">
                                    <span>Giriş</span>
                                </a>
                                <a href="#" class="sign-up">
                                    <span>Qeydiyyat</span>
                                </a>
                            </div>
                            <div class="user-form-body">
                                <div class="login-forms">
                                    <form action="" id="login-form">
                                        <div class="input-items">
                                            <div class="form-group">
                                                <input type="text" class="inputValidate" name="login_email" id="login_email">
                                                <label class="place-label">
                                                    E-poçt
                                                    <span>*</span>
                                                </label>
                                            </div>
                                    
                                            <div class="form-group password-input">
                                                <input type="password" minlength="8" class="inputValidate" name="login_password" id="login_password">
                                                <label class="place-label">
                                                    Şifrə
                                                    <span>*</span>
                                                </label>
                                                <label class="password-visibility">
                                                    <a href="#">
                                                        <img class="open-eye" src="./img2/password-open-eye.svg" alt="">
                                                        <img class="close-eye" src="./img2/password-close-eye.svg" style="display: none;" alt="">
                                                    </a>
                                                </label>
                                            </div>
                                        </div>
                                        <label class="error-message">
                                            E-poçt və ya şifrəniz yanlışdır.
                                        </label>
                        
                                        <div class="login-buton user-form-btn">
                                            <button type="submit">
                                                Giriş et
                                            </button>
                                        </div>
                                        <div class="forget-password">
                                            <a href="#">
                                                <span>
                                                    Şifrəni unutdum
                                                </span>
                                            </a>
                                        </div>
                        
                                        <div class="with-other-account">
                                            <a href="#">
                                                <div class="other-account-item">
                                                    <img src="./img2/login-fb-icon.svg" alt="">
                                                    <span>Facebook-la giriş et</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="other-account-item">
                                                    <img src="./img2/login-google-icon.svg" alt="">
                                                    <span>Google-lə giriş et</span>
                                                </div>
                                            </a>
                                        </div>
                                    </form>
                    
                                    <form action="" id="again-password" style="display: none;">
                                        <div class="input-items">
                                            <div class="form-group password-input">
                                                <input type="password" class="inputValidate" name="new-password" id="new-password" required>
                                                <label class="place-label">
                                                    Yeni şifrə
                                                    <span>*</span>
                                                </label>
                                                <label class="password-visibility">
                                                    <a href="#">
                                                        <img class="open-eye" src="./img2/password-open-eye.svg" alt="">
                                                        <img class="close-eye" src="./img2/password-close-eye.svg" style="display: none;" alt="">
                                                    </a>
                                                </label>
                                            </div>
                    
                                            <div class="form-group password-input">
                                                <input type="password" class="inputValidate" name="new-repassword" id="new-repassword" required>
                                                <label class="place-label">
                                                    Yeni şifrəni təkrarla
                                                    <span>*</span>
                                                </label>
                                                <label class="password-visibility">
                                                    <a href="#">
                                                        <img class="open-eye" src="./img2/password-open-eye.svg" alt="">
                                                        <img class="close-eye" src="./img2/password-close-eye.svg" style="display: none;" alt="">
                                                    </a>
                                                </label>
                                                <label class="min-password">Şifrəniz minimum 8 simvol ola bilər.</label>
                                            </div>
                                            
                                        </div>
                    
                                        <label class="error-message" style="display: none;">
                                            Şifrəniz eyni deyil.
                                        </label>
                        
                                        <div class="login-buton user-form-btn">
                                            <button type="submit">
                                                Giriş et
                                            </button>
                                        </div>
                                       
                                        <div class="with-other-account">
                                            <a href="#">
                                                <div class="other-account-item">
                                                    <img src="./img2/login-fb-icon.svg" alt="">
                                                    <span>Facebook-la giriş et</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="other-account-item">
                                                    <img src="./img2/login-google-icon.svg" alt="">
                                                    <span>Google-lə giriş et</span>
                                                </div>
                                            </a>
                                        </div>
                                    </form>
                                </div>
                                
                        
                                <form action=""  id="signup-form" style="display: none;">
                                    <div class="input-items">
                                        <div class="form-group">
                                            <input type="number" class="inputValidate number_input" name="signup_number" id="signup_number" minlength="10" required>
                                            <label class="place-label">
                                                Mobil nömrə
                                                <span>*</span>
                                            </label>
                                        </div>
                                
                                        <div class="form-group">
                                            <input type="text" class="inputValidate" name="signup_email" id="signup_email" required>
                                            <label class="place-label">
                                                E-poçt
                                                <span>*</span>
                                            </label>
                                        </div>
                                
                                        <div class="form-group">
                                            <input type="text" class="inputValidate" name="signup_username" id="signup_username" required>
                                            <label class="place-label">
                                                İstifadəçi adı
                                                <span>*</span>
                                            </label>
                                        </div>
                                        
                                        <div class="form-group password-input">
                                            <input type="password" class="inputValidate" name="signup_password" id="signup_password" minlength="8" required>
                                            <label class="place-label">
                                                Şifrə
                                                <span>*</span>
                                            </label>
                                            <label class="min-password">Şifrəniz minimum 8 simvol ola bilər.</label>
                                            <label class="error-message" style="display: none;">
                                                Şifrəniz eyni deyil.
                                            </label>
                                        </div>
                    
                                    </div>
                                    
                    
                                    <div class="signup-buton user-form-btn">
                                        <button type="submit">
                                            Qeydiyyatdan keç
                                        </button>
                                    </div>
                    
                                    <div class="with-other-account">
                                        <a href="#">
                                            <div class="other-account-item">
                                                <img src="./img2/login-fb-icon.svg" alt="">
                                                <span>Facebook-la giriş et</span>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="other-account-item">
                                                <img src="./img2/login-google-icon.svg" alt="">
                                                <span>Google-lə giriş et</span>
                                            </div>
                                        </a>
                                    </div>
                                </form>
                            </div>
                           
                            
                           
                        </div>
                    
                        <div class="user-form update-password" style="display: none;">
                            <div class="user-form-header">
                                <div class="title">
                                    Şifrəni yenilə
                                </div>
                                <p>Şifrənin yenilənməsi üçün e-poçt ünvanınızı qeyd edin.</p>
                            </div>
                            <div class="user-form-body">
                                <form id="update-password-form">
                                    <div class="input-items">
                                        <div class="form-group">
                                            <input type="text" class="inputValidate" name="forget_email" id="forget_email" required>
                                            <label class="place-label">
                                                E-poçt
                                                <span>*</span>
                                            </label>
                                        </div>                   
                                    </div>
                                    
                                    <div class="update-password-btn user-form-btn">
                                        <button type="submit">
                                            Şifrəni yenilə
                                        </button>
                                    </div>
                                </form>
                               
                            </div>
                        </div>
                
                        <div class="user-form again-mail" style="display: none;">
                            <div class="user-form-header">
                                <div class="title">
                                    Şifrəniz göndərildi
                                </div>
                                <p>Firengizbn@code.edu.az e-poçt ünvanınıza şifrə yenilənməsi linki göndərildi.</p>
                            </div>
                            <div class="user-form-body">
                                <form>
                
                                    <div class="again-login-btn user-form-btn">
                                        <button type="submit">
                                            Giriş et
                                        </button>
                                    </div>
                
                                    <div class="again-send-btn">
                                        <button type="submit">
                                            Təkrar göndər.
                                        </button>
                                    </div>
                                    
                                    
                                </form>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php
            include("includes/footer.php");
        ?>
    </section>
   
<?php
    include("includes/script.php");
?>