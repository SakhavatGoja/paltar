<?php
    include("includes/head.php");
?>

    <section class="user-info">
        <?php
            include("includes/header.php");
        ?>
        <div class="user-info-container">
            <div class="container">
                <div class="row">
                    <div class="user-info-box">
                        <div class="user-info-left">
                            <ul>
                                <li class="account-info active">
                                    <a href="#">
                                        <p>Hesabım</p>
                                        <img src="./img2/left-arrow.svg" alt="">
                                    </a>
                                </li>
                                <li class="order-history">
                                    <a href="#">
                                        <p>Sifariş tarixçəsi</p>
                                        <img src="./img2/left-arrow.svg" alt="">
                                    </a>
                                </li>
                                <li class="account-logout">
                                    <a href="#">
                                        <p>Çıxış</p>
                                        <img src="./img2/left-arrow.svg" alt="">
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="user-info-right">
                            <div class="account-info-part">
                                <p>
                                    Şəxşi məlumatlar
                                </p>
                                <form action="" id="user-info-form">
                                    <div class="input-items">
                                        <div class="form-group">
                                            <input type="text" class="inputValidate" name="name" id="name" required>
                                            <label class="place-label">
                                                Ad
                                                <span>*</span>
                                            </label>
                                        </div>
    
                                        <div class="form-group">
                                            <input type="text" class="inputValidate" name="surname" id="surname" required>
                                            <label class="place-label">
                                                Soyad
                                                <span>*</span>
                                            </label>
                                        </div>
    
                                        <div class="form-group date-input">
                                            <input type="text" class="inputValidate" name="date" id="date">
                                            <label class="place-label">
                                                Doğum tarixi
                                            </label>
                                            <label class="date-icon">
                                                <a href="#">
                                                    <img src="./img2/date.svg" alt="">
                                                </a> 
                                            </label>
                                        </div>
    
                                        <div class="form-group">
                                            <input type="text" class="inputValidate" name="email" id="email" required>
                                            <label class="place-label">
                                                E-poçt
                                                <span>*</span>
                                            </label>
                                        </div>
    
                                        <div class="form-group">
                                            <input type="number" class="inputValidate number_input" name="phone_number" id="phone_number" minlength="10" required>
                                            <label class="place-label">
                                                Mobil nömrə
                                                <span>*</span>
                                            </label>
                                        </div>
    
                                        <div class="form-group">
                                            <select name="city[]" class="nice-select inputValidate" required>
                                                <option value= "" disabled selected> Şəhər</option>
                                                <option value="1">Bakı</option>
                                                <option value="2">Sumqayıt</option>
                                                <option value="3">Gəncə</option>
                                                <option value="4">Mingəçevir</option>
                                            </select>
                                        </div>
                                
                                        <div class="form-group">
                                            <input type="text" class="inputValidate" name="address" id="address" required>
                                            <label class="place-label">
                                                Ünvan
                                                <span>*</span>
                                            </label>
                                        </div>
    
                                        <div class="form-group">
                                            <input type="text" class="inputValidate" name="other_address" id="other_address">
                                            <label class="place-label">
                                                Ünvan
                                            </label>
                                        </div>
    
                                        
                                        
                                    </div>
    
                                    <div class="other-input-item">
                                        <p>Əlavə</p>
                                        <div class="form-group">
                                            <select name="lang[]" class="nice-select inputValidate">
                                                <option value= "" disabled selected> Dili seçin</option>
                                                <option value="1">Az</option>
                                                <option value="2">Eng</option>
                                                <option value="3">Ru</option>
                                            </select>
                                        </div>
                                    </div>
    
                                    <div class="form-footer">
                                        <div class="save-btn">
                                            <button type="submit">
                                                Yadda saxla
                                            </button>
                                        </div>
        
                                        <div class="edit-btn">
                                            <button type="submit">
                                                Dəyişdir
                                            </button>
                                        </div>
                                    </div>
                                    
                                </form>
                            </div>
                            <div class="order-history-part" style="display: none;">
                                <div class="title">
                                    Son əməliyyatlar
                                </div>
                                <div class="order-date-filter">
                                    <div class="otder-date-item">
                                        <label class="text">Başlama</label>
                                        <div class="order-date-input">
                                            <input type="date" id="start-order-date">
                                            <label class="date-icon"><img src="./img2/date.svg" alt=""></label>
                                        </div>
                                    </div>

                                    <div class="otder-date-item">
                                        <label class="text">Bitmə</label>
                                        <div class="order-date-input">
                                            <input type="date" id="end-order-date">
                                            <label class="date-icon"><img src="./img2/date.svg" alt=""></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="order-invoice-table">
                                    <ul>
                                        <li class="table-header table-item invoice-detail-link">
                                            <a href="#">
                                                <ul>
                                                    <li>Tarix</li>
                                                    <li>Ödəniş tipi</li>
                                                    <li>Əməliyyat məbləği</li>
                                                    <li>Status</li>
                                                </ul>
                                            </a>
                                        </li>
                                      
                                        <li class="table-item invoice-detail-link">
                                            <a href="#">
                                                <ul>
                                                    <li>22.11.21</li>
                                                    <li>Kartla</li>
                                                    <li>2302 manat</li>
                                                    <li><img src="./img2/waiting-icon.svg" alt=""> <p>Sifariş gözlənilir</p></li>
                                                </ul>
                                            </a>
                                        </li>
                                        <li class="table-item invoice-detail-link">
                                            <a href="#">
                                                <ul>
                                                    <li>11.11.21</li>
                                                    <li>Nağd</li>
                                                    <li>2302 manat</li>
                                                    <li><img src="./img2/delivered-icon.svg" alt=""> <p>Sifariş çatdırıldı</p></li>
                                                </ul>
                                            </a>
                                        </li>
                                        <li class="table-item invoice-detail-link">
                                            <a href="#">
                                                <ul>
                                                    <li>11.11.21</li>
                                                    <li>Nağd</li>
                                                    <li>2302 manat</li>
                                                    <li><img src="./img2/process-icon.svg" alt=""> <p>Sifariş icradadır</p></li>
                                                </ul>
                                            </a>
                                        </li>
                                        <li class="table-item invoice-detail-link">
                                            <a href="#">
                                                <ul>
                                                    <li>11.11.21</li>
                                                    <li>Nağd</li>
                                                    <li>2302 manat</li>
                                                    <li><img src="./img2/cancel-icon.svg" alt=""> <p>Sifariş ləğv edildi</p></li>
                                                </ul>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="scroll-text"><img src="./img2/scroll_x-icon.svg" alt=""><p>Sola çək</p></div>

                            </div>

                            <div class="invoice-details" style="display: none;">
                                <div class="main-title">
                                    İnvoice
                                </div>
                                <div class="detail-info">
                                    <div class="user-invoice-info">
                                        <div class="title">Alıcı haqqında</div>
                                        <p class="name">Fira Nuriyeva</p>
                                        <p class="main-address">Bakı, Azərbaycan</p>
                                        <p class="other-address">Bakı, Azərbaycan</p>
                                    </div>
                                    <div class="invoice-id">
                                        <div class="title">İnvoice nömrəsi</div>
                                        <p>000202000202</p>
                                    </div>
                                    <div class="invoice-date">
                                        <div class="title">Sifariş tarixi</div>
                                        <p>17.01.1891</p>
                                    </div>
                                    <div class="invoice-price">
                                        <div class="title">Toplam İnvoice</div>
                                        <p>4304 &#8380;</p>
                                    </div>
                                </div>

                                
                                <div class="invoice-detail-table">
                                    <ul>
                                        <li class="table-header table-item">
                                            <a href="#">
                                                <ul>
                                                    <li>Məhsulun adı</li>
                                                    <li>Kodu</li>
                                                    <li>Sayı</li>
                                                    <li>Ölçüsü</li>
                                                    <li>Qiyməti</li>
                                                </ul>
                                            </a>
                                        </li>
                                      
                                        <li class="table-item">
                                            <a href="#">
                                                <ul>
                                                    <li>Vilvet uzunqol köynek</li>
                                                    <li>2233445566</li>
                                                    <li>2</li>
                                                    <li>S</li>
                                                    <li>2302 &#8380;</li>
                                                </ul>
                                            </a>
                                        </li>
                                        <li class="table-item">
                                            <a href="#">
                                                <ul>
                                                    <li>Vilvet uzunqol köynek</li>
                                                    <li>2233445566</li>
                                                    <li>12</li>
                                                    <li>S</li>
                                                    <li>2302 &#8380;</li>
                                                </ul>
                                            </a>
                                        </li>
                                        <li class="table-item">
                                            <a href="#">
                                                <ul>
                                                    <li>Vilvet uzunqol köynek</li>
                                                    <li>2233445566</li>
                                                    <li>2</li>
                                                    <li>S</li>
                                                    <li>2302 &#8380;</li>
                                                </ul>
                                            </a>
                                        </li>
                                        <li class="table-item">
                                            <a href="#">
                                                <ul>
                                                    <li>Vilvet uzunqol köynek</li>
                                                    <li>2233445566</li>
                                                    <li>1000</li>
                                                    <li>XL</li>
                                                    <li>2302 &#8380;</li>
                                                </ul>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="scroll-text"><img src="./img2/scroll_x-icon.svg" alt=""><p>Sola çək</p></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php
            include("includes/footer.php");
        ?>
    </section>

<?php
    include("includes/script.php");
?>

