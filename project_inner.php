<?php
    include("includes/head.php");
?>



<section class="project_inner">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
        
        <div class="container">
          <div class="row">
              <div class="project_inner_container w-100">
                <div class="breadcrumbs">
                    <span class="old_page">Azclimart</span>
                    <img src="img/breadcrumb.svg" alt="">
                    <span class="old_page">Corella boyaları</span>
                    <img src="img/breadcrumb.svg" alt="">
                    <a href="#" class="new_page">Emusiyalar</a>
                </div>
                <div class="project_top_container">
                    <div class="product-img">
                        <button type="button" class="project_inner_img">
                            <div class="project_loop_box">
                                <img src="img2/product-img1.png" alt="" class="zoom_img">
                            </div>
                        </button>
                        <div class="other-product-imgs">
                            <div class="other-imgs owl-carousel owl-theme">
                                <div class="other-img-item item"><img src="./img2/product-img1.png" alt=""></div>
                                <div class="other-img-item item"><img src="./img2/other-img2.png" alt=""></div>
                                <div class="other-img-item item"><img src="./img2/other-img3.png" alt=""></div>
                                <div class="other-img-item item"><img src="./img2/other-img4.png" alt=""></div>
                                <div class="other-img-item item"><img src="./img2/other-img4.png" alt=""></div>
                                <div class="other-img-item item"><img src="./img2/other-img4.png" alt=""></div>
                                <div class="other-img-item item"><img src="./img2/other-img5.png" alt=""></div>
                                <div class="other-img-item item"><img src="./img2/other-img5.png" alt=""></div>
                                <div class="other-img-item item"><img src="./img2/other-img5.png" alt=""></div>

                            </div>
                            
                        </div>
                    </div>
                       
                    
                    <div class="project_inner_detail">
                        <p class="project_name">Benisengiydir Kadın Antrasit 50 Management Baskılı Oversize Sweatshirt </p>
                        <div class="project_inner_middle">
                            
                            <div class="project_inner_number">
                                <p>Məhsul kodu: </p>
                                <p> 084883882828288299</p>
                            </div>

                            <p class="title">Bədən</p>
                            <div class="body_size">
                                <div class="body_size_item">
                                    XS
                                </div>

                                <div class="body_size_item">
                                    S
                                </div>

                                <div class="body_size_item">
                                    M
                                </div>

                                <div class="body_size_item">
                                    L
                                </div>

                                <div class="body_size_item">
                                    XL
                                </div>
                            </div>


                            <p class="title">Rəng</p>

                            <div class="product_color">

                                <span class="product_color_item">
                                    
                                </span>

                                <span class="product_color_item">
                                    
                                </span>

                                <span class="product_color_item">
                                    
                                </span>

                                <span class="product_color_item">
                                    
                                </span>
                            </div>

                            <div class="project_inner_operation">
                                <div class="inner_count_box" data-target="inner_price">
                                    <button class="minus">-</button>
                                    <input type="number" id="inner_price" value="1" name="" min="1">
                                    <button class="plus">+</button>
                                </div>
                                <div class="product_price">
                                    <div class="current_price">
                                        <span class="price" data-price="75.8">75.80</span>₼
                                    </div>
                                    <div class="prev_price">
                                        <p class="price" data-price="75.8">320.50</p>₼
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="project_inner_btns">
                            <button class="add-basketbox">Səbətə əlavə et</button>
                            <button class="add-wishlist">
                                <svg width="28" height="27" viewBox="0 0 28 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12.6648 23.6539L12.6634 23.6526C9.07882 20.4021 6.18469 17.7755 4.17466 15.318C2.17612 12.8746 1.15625 10.7206 1.15625 8.43288C1.15625 4.72165 4.05844 1.81946 7.76968 1.81946C9.8755 1.81946 11.9122 2.80464 13.2375 4.3609L13.9988 5.25496L14.7602 4.3609C16.0854 2.80464 18.1222 1.81946 20.228 1.81946C23.9392 1.81946 26.8414 4.72165 26.8414 8.43288C26.8414 10.7207 25.8216 12.8747 23.8228 15.32C21.8127 17.7793 18.9187 20.4092 15.3342 23.6665C15.3341 23.6666 15.3341 23.6667 15.334 23.6667L15.3335 23.6672L14.0014 24.8706L12.6648 23.6539Z" stroke="#A13670" stroke-width="2"/>
                                </svg>
                            </button>
                        </div>


                        <div class="product_info">
                            <p class="title">Məhsul haqqında</p>

                            <ul>
                                <li>Xena Kadın Siyah Baskılı Polarlı Sweatshirt 1KZK8-11232-02</li>
                                <li>Kumaş Bilgileri : %60 Pamuk %40 Polyester</li>
                                <li>Manken bilgileri : Boy : 1.70</li>
                                <li>
                                    <ul>
                                        <li><span></span> <p>Göğüs : 83</p</li>
                                        <li><span></span> <p>Bel : 62</p></li>
                                        <li><span></span> <p>Kalça : 90</p></li>
                                    </ul>
                                </li>
                                <li>Bu üründen en fazla 10 adet sipariş verilebilir. 10 adetin üzerindeki siparişleri Trendyol iptal etme hakkını saklı tutar.</li>
                                <li>Kampanya fiyatından satılmak üzere 100 adetten fazla stok sunulmuştur.</li>
                                <li>Listelenen fiyat 22 Kasım 2021 tarihine kadar geçerlidir. Bu ürün indirim kampanyasına dahil değildir.</li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="project_inner_table_info">
                  <div class="project_inner_heading">
                    <p>Göstəriciləri</p>
                    <p>Əmsallar</p>
                  </div>
                  <div class="project_inner_body">
                    <div class="project_body_row">
                      <p>Stil</p>
                      <p>Trend</p>
                    </div>
                    <div class="project_body_row">
                      <p>Sürdürülebilir</p>
                      <p>Geri Dönüştürülmüş Metaryal</p>
                    </div>
                    <div class="project_body_row">
                      <p>Kumaş Tipi</p>
                      <p>Dokuma</p>
                    </div>
                    <div class="project_body_row">
                      <p>Boy / Ölçü</p>
                      <p>Orta</p>
                    </div>
                  </div>
                </div>

                
              </div>
          </div>
        </div>
    </div>
    <div class="modal fade" id="imgShow" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button class="delete" data-dismiss="modal" aria-label="Close"><img src="img/esc.svg" alt=""></button>
                <div class="modal-body owl-carousel owl-theme">
                    <div class="modal-img item"><img src="" alt=""></div>
                    <div class="item"><img src="./img2/product-img1.png" alt=""></div>
                    <div class="item"><img src="./img2/other-img2.png" alt=""></div>
                    <div class="item"><img src="./img2/other-img3.png" alt=""></div>
                    <div class="item"><img src="./img2/other-img4.png" alt=""></div>
                    <div class="item"><img src="./img2/other-img4.png" alt=""></div>
                    <div class="item"><img src="./img2/other-img4.png" alt=""></div>
                    <div class="item"><img src="./img2/other-img5.png" alt=""></div>
                </div>
                
            </div>
        </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
