<?php
    include("includes/head.php");
?>


<section class="index">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
        <div class="index-landing ">
          <div class="landing_left" style="background-image: url('./img/left_img.png')">
            <div class="left_absolute">
              <p class="heading_left">Yeni sezon</p>
              <a href="#" class="go_pay">Alış-verişə başla</a>
            </div>
          </div>
          <div class="landing_right">
            <a href="#" class="right_rectangular" style="background-image: url('./img/model1.jpg')">
              <p class="header_rec">Bütün məhsullara payız endirimi</p>
            </a>
            <a href="#" class="right_rectangular" style="background-image: url('./img/model1.jpg')">
              <p class="header_rec">Bütün məhsullara payız endirimi</p>
            </a>
            <a href="#" class="right_rectangular" style="background-image: url('./img/model1.jpg')">
              <p class="header_rec">Bütün məhsullara payız endirimi</p>
            </a>
            <a href="#" class="right_rectangular" style="background-image: url('./img/model1.jpg')">
              <p class="header_rec">Bütün məhsullara payız endirimi</p>
            </a>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="repeat_main_container">
              <div class="same_bottom_container galeryBox">
                    <div class="container_4_grid">
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
              </div>
              
              <div class="same_bottom_container">
                  <div class="heading_container_same">
                      <p class="title_same_heading">Sizə özəl məhsullar</p>
                      <a href="#" class="see_all">Hamısına bax</a>
                  </div>
                    <div class="container_4_grid">
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
              </div>
              
              <div class="same_bottom_container">
                  <div class="heading_container_same">
                      <p class="title_same_heading">Sizə özəl məhsullar</p>
                  </div>
                    <div class="container_4_grid">
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <div class="paint_box">
                            <div class="discount">
                                15 % endirimlə
                            </div>
                            <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                            <div class="paint_desc">
                                <p>
                                Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                </p>
                            </div>
                            <div class="paint_prices">
                                <div class="price_box">
                                    <p class="current_price">75.80₼</p>
                                    <p class="old_price">63.50₼</p>
                                </div>
                                <button type="button" class="heart">
                                    <div class="heart_center">
                                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                    <button class="see_more">Daha ətraflı</button>
              </div>
            </div>
          </div>
        </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
