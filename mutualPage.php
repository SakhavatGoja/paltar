<?php
    include("includes/head.php");
?>


<section class="mutualPage">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
        <div class="container">
            <div class="row">
                <div class="repeat_main_container">
                    <div class="breadcrumbs">
                        <a href="#" class="old_page">Yeni məhsullar</a>
                        <img src="img/breadcrumb.svg" alt="">
                        <span class="new_page">Azclimart</span>
                    </div>
                    <div class="repeat_filter_box">
                        <div class="heading_container_same">
                            <p class="title_same_heading">Şalvar</p>
                            <button class="filter"><img src="img/filter.svg" alt=""></button>
                        </div>
                        <div class="filter_grid">
                            <div class="paint_main_grid">
                                <div class="paints_grid">
                                    <div class="paint_box">
                                        <div class="discount">
                                            15 % endirimlə
                                        </div>
                                        <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                                        <div class="paint_desc">
                                            <p>
                                            Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                            </p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="heart">
                                                <div class="heart_center">
                                                    <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                                    </svg>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <div class="discount">
                                            15 % endirimlə
                                        </div>
                                        <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                                        <div class="paint_desc">
                                            <p>
                                            Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                            </p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="heart">
                                                <div class="heart_center">
                                                    <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                                    </svg>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <div class="discount">
                                            15 % endirimlə
                                        </div>
                                        <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                                        <div class="paint_desc">
                                            <p>
                                            Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                            </p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="heart">
                                                <div class="heart_center">
                                                    <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                                    </svg>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <div class="discount">
                                            15 % endirimlə
                                        </div>
                                        <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                                        <div class="paint_desc">
                                            <p>
                                            Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                            </p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="heart">
                                                <div class="heart_center">
                                                    <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                                    </svg>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <div class="discount">
                                            15 % endirimlə
                                        </div>
                                        <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                                        <div class="paint_desc">
                                            <p>
                                            Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                            </p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="heart">
                                                <div class="heart_center">
                                                    <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                                    </svg>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <div class="discount">
                                            15 % endirimlə
                                        </div>
                                        <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                                        <div class="paint_desc">
                                            <p>
                                            Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                            </p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="heart">
                                                <div class="heart_center">
                                                    <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                                    </svg>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <div class="discount">
                                            15 % endirimlə
                                        </div>
                                        <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                                        <div class="paint_desc">
                                            <p>
                                            Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                            </p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="heart">
                                                <div class="heart_center">
                                                    <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                                    </svg>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="paint_box">
                                        <div class="discount">
                                            15 % endirimlə
                                        </div>
                                        <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                                        <div class="paint_desc">
                                            <p>
                                            Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                            </p>
                                        </div>
                                        <div class="paint_prices">
                                            <div class="price_box">
                                                <p class="current_price">75.80₼</p>
                                                <p class="old_price">63.50₼</p>
                                            </div>
                                            <button type="button" class="heart">
                                                <div class="heart_center">
                                                    <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                                    </svg>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <button class="see_more">Daha ətraflı</button>
                            </div>
                            <aside class="aside-form">
                                <button class="esc"><img src="img/esc.svg" alt=""></button>
                                <form action="" class="left_filter_box">
                                    <div class="filter_header">
                                        <p class="filter_title">Filter</p>
                                        <button id="clean">Təmizlə</button>
                                    </div>
                                    <div class="filter_operation">
                                        <div class="filter_special_box">
                                            <div class="f_spe_heading">
                                                <p>Rəng</p>
                                                <img src="img/filter_arrow.svg" alt="">
                                            </div>
                                            <div class="block">
                                                <div class="label_box">
                                                    <div class="label_single">
                                                        <input type="checkbox" name="white" id="white">
                                                        <label for="white" class="label_project">Ağ</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="brown" id="brown">
                                                        <label for="brown" class="label_project">Qəhvəyi</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="purple" id="purple">
                                                        <label for="purple" class="label_project">Bənövşəyi</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="blue" id="blue">
                                                        <label for="blue" class="label_project">Göy</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="yellow" id="yellow">
                                                        <label for="yellow" class="label_project">Sarı</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="pink" id="pink">
                                                        <label for="pink" class="label_project">Çəhrayı</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="black" id="black">
                                                        <label for="black" class="label_project">Qara</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="green" id="green">
                                                        <label for="green" class="label_project">Yaşıl</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="filter_special_box">
                                            <div class="f_spe_heading">
                                                <p>Bədən</p>
                                                <img src="img/filter_arrow.svg" alt="">
                                            </div>
                                            <div class="block">
                                                <div class="label_box">
                                                    <div class="label_single">
                                                        <input type="checkbox" name="xxs" id="xxs">
                                                        <label for="xxs" class="label_project">XXS</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="xs" id="xs">
                                                        <label for="xs" class="label_project">XS</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="small" id="small">
                                                        <label for="small" class="label_project">S</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="medium" id="medium">
                                                        <label for="medium" class="label_project">M</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="large" id="large">
                                                        <label for="large" class="label_project">L</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="xl" id="xl">
                                                        <label for="xl" class="label_project">XL</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="filter_special_box">
                                            <div class="f_spe_heading">
                                                <p>Qiymət</p>
                                                <img src="img/filter_arrow.svg" alt="">
                                            </div>
                                            <div class="block range-block">
                                                <div class="range-line"></div>
                                                <input type="text" class="js-range-slider" name="my_range" value="" />
                                                <div class="label_box">
                                                    <div class="label_single">
                                                        <input type="checkbox" name="50-100" id="50-100">
                                                        <label for="50-100" class="label_project">50-100 manat aralığı</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="100-200" id="100-200">
                                                        <label for="100-200" class="label_project">100-200 manat aralığı</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="200-300" id="200-300">
                                                        <label for="200-300" class="label_project">200-300 manat aralığı</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="300-500" id="300-500">
                                                        <label for="300-500" class="label_project">300-500 manat aralığı</label>
                                                    </div>
                                                </div>
                                            </div>     
                                        </div>
                                        <div class="filter_special_box">
                                            <div class="f_spe_heading">
                                                <p>Populyar filterlər</p>
                                                <img src="img/filter_arrow.svg" alt="">
                                            </div>
                                            <div class="block">
                                                <div class="label_box">
                                                    <div class="label_single">
                                                        <input type="checkbox" name="new_season" id="new_season">
                                                        <label for="new_season" class="label_project">Yeni sezon</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="most_selled" id="most_selled">
                                                        <label for="most_selled" class="label_project">Ən çox satan</label>
                                                    </div>
                                                    <div class="label_single">
                                                        <input type="checkbox" name="summer_discount" id="summer_discount">
                                                        <label for="summer_discount" class="label_project">Yay endirimi</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </aside>
                        </div>
                    </div>
                    <div class="same_bottom_container same_swiper">
                        <div class="heading_container_same">
                            <p class="title_same_heading">Çox satan</p>
                            <a href="#" class="see_all">Hamısına bax</a>
                        </div>
                        <div class="container_4_grid">
                            <div class="paint_box">
                                <div class="discount">
                                    15 % endirimlə
                                </div>
                                <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                                <div class="paint_desc">
                                    <p>
                                    Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                    </p>
                                </div>
                                <div class="paint_prices">
                                    <div class="price_box">
                                        <p class="current_price">75.80₼</p>
                                        <p class="old_price">63.50₼</p>
                                    </div>
                                    <button type="button" class="heart">
                                        <div class="heart_center">
                                            <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                            </svg>
                                        </div>
                                    </button>
                                </div>
                            </div>
                            <div class="paint_box">
                                <div class="discount">
                                    15 % endirimlə
                                </div>
                                <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                                <div class="paint_desc">
                                    <p>
                                    Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                    </p>
                                </div>
                                <div class="paint_prices">
                                    <div class="price_box">
                                        <p class="current_price">75.80₼</p>
                                        <p class="old_price">63.50₼</p>
                                    </div>
                                    <button type="button" class="heart">
                                        <div class="heart_center">
                                            <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                            </svg>
                                        </div>
                                    </button>
                                </div>
                            </div>
                            <div class="paint_box">
                                <div class="discount">
                                    15 % endirimlə
                                </div>
                                <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                                <div class="paint_desc">
                                    <p>
                                    Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                    </p>
                                </div>
                                <div class="paint_prices">
                                    <div class="price_box">
                                        <p class="current_price">75.80₼</p>
                                        <p class="old_price">63.50₼</p>
                                    </div>
                                    <button type="button" class="heart">
                                        <div class="heart_center">
                                            <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                            </svg>
                                        </div>
                                    </button>
                                </div>
                            </div>
                            <div class="paint_box">
                                <div class="discount">
                                    15 % endirimlə
                                </div>
                                <div class="paint_img"><img src="img/girl1.png" alt=""></div>
                                <div class="paint_desc">
                                    <p>
                                    Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR Kondisioner AUX Aswho9a4RR
                                    </p>
                                </div>
                                <div class="paint_prices">
                                    <div class="price_box">
                                        <p class="current_price">75.80₼</p>
                                        <p class="old_price">63.50₼</p>
                                    </div>
                                    <button type="button" class="heart">
                                        <div class="heart_center">
                                            <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M10.1022 17.9597L10.1007 17.9584C7.29104 15.4106 5.04014 13.3669 3.47962 11.4591C1.93058 9.56522 1.1665 7.92735 1.1665 6.20833C1.1665 3.42395 3.34046 1.25 6.12484 1.25C7.70705 1.25 9.24125 1.99144 10.2385 3.16251L10.9998 4.05657L11.7612 3.16251C12.7584 1.99144 14.2926 1.25 15.8748 1.25C18.6592 1.25 20.8332 3.42395 20.8332 6.20833C20.8332 7.92736 20.0691 9.56528 18.5198 11.4607C16.9592 13.37 14.7085 15.4161 11.8989 17.9693C11.8986 17.9695 11.8984 17.9697 11.8981 17.9699L11.0024 18.7792L10.1022 17.9597Z" stroke="#A13670" stroke-width="2"/>
                                            </svg>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
