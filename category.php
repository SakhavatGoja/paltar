<?php
    include("includes/head.php");
?>


<section class="category">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
      <div class="container">
        <div class="row">
            <div class="category_main_container w-100">
                <div class="breadcrumbs">
                  <a href="#" class="new_page">Azclimart</a>
                  <img src="img/breadcrumb.svg" alt="">
                  <span class="old_page">Yeni məhsullar</span>
                </div>
                <div class="heading_container_same">
                    <p class="title_same_heading">Qadın geyimləri</p>
                </div>
                <div class="category_list">
                  <a href="#"><img src="img/category.png" alt=""></a>
                  <a href="#"><img src="img/category.png" alt=""></a>
                  <a href="#"><img src="img/category.png" alt=""></a>
                  <a href="#"><img src="img/category.png" alt=""></a>
                  <a href="#"><img src="img/category.png" alt=""></a>
                  <a href="#"><img src="img/category.png" alt=""></a>
                  <a href="#"><img src="img/category.png" alt=""></a>
                  <a href="#"><img src="img/category.png" alt=""></a>
                  <a href="#"><img src="img/category.png" alt=""></a>
                </div>
            </div>
        </div>
      </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
