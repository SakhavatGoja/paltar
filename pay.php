<?php
    include("includes/head.php");
?>


<section class="pay">
    <?php
        include("includes/header.php");
    ?>
    <div class="main_section_container">
      <div class="container">
        <div class="row">
            <div class="pay_main_container">
                <div class="breadcrumbs">
                  <a href="#" class="new_page">Azclimart</a>
                  <img src="img/breadcrumb.svg" alt="">
                  <span class="old_page">Yeni məhsullar</span>
                </div>
                <div class="heading_container_same">
                    <p class="title_same_heading">Sifarişi tamamla</p>
                </div>
                <form action="" id="pay_form">
                  <div class="contact_form_container">
                    <div class="fraction-form">
                      <p class="form_title">Əlaqə məlumatları</p>
                      <div class="contact_form_box">
                        <div class="contact_left">
                          <div class="form-group" >
                            <input class="inputValidate" type="text" name="name" required>
                            <label class="place-label">Ad <span>*</span></label>
                          </div>
                          <div class="form-group" >
                            <input class="inputValidate" type="text" name="surname" required>
                            <label class="place-label">Soyad <span>*</span></label>
                          </div>
                          <div class="form-group" >
                            <input class="inputValidate" type="text" name="father_name" required>
                            <label class="place-label">Ata adı <span>*</span></label>
                          </div>
                          <div class="form-group">
                            <input class="inputValidate number_input" type="number" minlength="10" name="phone" required>
                            <label class="place-label">Telefon nömrəsi <span>*</span></label>
                          </div>
                        </div>
                        <div class="contact_right">
                          <div class="form-group" >
                            <input class="inputValidate" type="email" name="email" required>
                            <label class="place-label">Email <span>*</span></label>
                          </div>
                          <div class="form-group">
                            <textarea name="textarea"></textarea>
                            <label class="place-label">Əlavə qeyd </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="middle_fraction_container">
                      <div class="fraction-form">
                        <p class="form_title">Çatdırılma ünvanı</p>
                        <div class="contact_form_box">
                          <div class="form-group">
                            <select name="city[]" class="nice-select inputValidate" required>
                                <option value= "" disabled selected> Şəhər</option>
                                <option value="1">Bakı</option>
                                <option value="2">Sumqayıt</option>
                                <option value="3">Gəncə</option>
                                <option value="4">Mingəçevir</option>
                            </select>
                          </div>
                          <div class="form-group" >
                              <input class="inputValidate" type="text" name="address" required>
                              <label class="place-label">Ünvan <span>*</span></label>
                            </div>
                        </div>
                      </div>
                      <div class="fraction-form radio-fraction">
                        <p class="form_title">Ödəmə üsulu</p>
                        <div class="contact_form_box">
                          <div class="form-group">

                            <div class="form-label">
                              <input type="radio" name="radioname" id="cash" />
                              <label for="cash" class="label-main">
                                <div class="label-img"><img src="img/cash.png" alt=""></div>
                                <p>Çatdırıldıqda nağd ödə</p>
                              </label>
                            </div>

                            <div class="form-label">
                              <input type="radio" name="radioname" id="online" />
                              <label for="online" class="label-main">
                                <div class="label-img"><img src="img/online.png" alt=""></div>
                                <p>Online ödə</p>
                              </label>
                            </div>

                            <div class="form-label">
                              <input type="radio" name="radioname" id="cart" />
                              <label for="cart" class="label-main">
                                <div class="label-img"><img src="img/cart.png" alt=""></div>
                                <p>Çatdırıldıqda kartı ilə ödə</p>
                              </label>
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-result-main-container">
                    <div class="form_result_box">
                      <div class="info_form_container">
                        <p class="title_result_form">Sifarişin qiyməti</p>
                        <div class="count_form_box">
                          <p class="count_product"><span>2</span>məhsul</p>
                          <p class="price_product same_result_text">
                            <span>7000</span>₼
                          </p>
                        </div>
                        <div class="delivery_form_box">
                          <p class="title_delivery">Çatdırılma</p>
                          <p class="delivery_price same_result_text">PULSUZ</p>
                        </div>
                      </div>
                      <div class="result_form_container">
                        <div class="box_summary">
                          <p class="title_summary">Ümumi ödəniləcək</p>
                          <p class="price_summary"><span>7000</span>₼ </p>
                        </div>
                        <button type="submit" class="btn_pink">Sifarişi tamamla</button>
                      </div>
                    </div>
                  </div>
                  
                </form>
            </div>
        </div>
      </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
